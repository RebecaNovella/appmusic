import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './@pages/login/login.component';
import { RegisterComponent } from './@pages/register/register.component';
import { HomeComponent } from './@pages/home/home.component';
import { CartComponent } from './@pages/cart/cart.component';
import { ProductCardComponent } from './@components/product-card/product-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { AlbumDetailsComponent } from './@pages/album-details/album-details.component';
import { HttpClientModule } from '@angular/common/http';
import { DireccionEnvioComponent } from './@pages/direccion-envio/direccion-envio.component';
import { DireccionFacturaComponent } from './@pages/direccion-factura/direccion-factura.component';
import { MetodoPagoComponent } from './@pages/metodo-pago/metodo-pago.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CartComponent,
    ProductCardComponent,
    AlbumDetailsComponent,
    DireccionEnvioComponent,
    DireccionFacturaComponent,
    MetodoPagoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
