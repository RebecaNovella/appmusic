import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './@pages/login/login.component';
import { RegisterComponent } from './@pages/register/register.component';
import { HomeComponent } from './@pages/home/home.component';
import { CartComponent } from './@pages/cart/cart.component';
import { AlbumDetailsComponent } from './@pages/album-details/album-details.component';
import { DireccionEnvioComponent } from './@pages/direccion-envio/direccion-envio.component';
import { DireccionFacturaComponent } from './@pages/direccion-factura/direccion-factura.component';
import { MetodoPagoComponent } from './@pages/metodo-pago/metodo-pago.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registrar', component: RegisterComponent },
  { path: 'detalles/:album', component: AlbumDetailsComponent },
  { path: '', component: HomeComponent },
  { path: 'compras', component: CartComponent },
  { path: 'envios', component: DireccionEnvioComponent },
  { path: 'facturas', component: DireccionFacturaComponent },
  { path: 'pagos', component: MetodoPagoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
