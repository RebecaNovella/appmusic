import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/@services/cart/cart.service';
import { Direccion, DireccionEnvioService } from 'src/app/@services/direccion-envio/direccion-envio.service';
import { Item } from 'src/app/@services/item/item.service';
import { MetodoPagoService, Tarjeta } from 'src/app/@services/metodo-pago/metodo-pago.service';
import { UserService } from 'src/app/@services/user/user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {

  items: Item[] = []
  cupon = ''
  tarjetas: Tarjeta[] = []
  facturas: Direccion[] = []
  envios: Direccion[] =[]

  constructor(
    private cartService: CartService,
    public dirService: DireccionEnvioService,
    public pagosService: MetodoPagoService,
    private router: Router,
    private userService: UserService,
  ) {

  }

  ngOnInit() {
    this.loadItems()
    this.pagosService.getTarjetas(this.userService.getActiveUser()!.id!).subscribe(tarjetas => this.tarjetas = tarjetas)
    this.dirService.getFacturas().subscribe(facturas => this.facturas = facturas)
    this.dirService.getEnvios().subscribe(envios => this.envios = envios)
  }

  removeFromCart(item:Item) {
    this.cartService.deleteFromCart(item)
    this.loadItems()
  }

  loadItems() {
    this.items = this.cartService.getItems();
  }

  ocultaTarjeta(numeroTarjeta:string) {
    return '********' + numeroTarjeta.slice(-4)
  }

  getTotal() {
    return this.items.reduce((prev,curr) => prev+curr.precio, 0)
  }
  
  aplicaCupon() {
    this.cupon = 'Cupón'
  }

  pagar() {
    this.cartService.clearCart()
    this.router.navigate(['/'])
  }
}
