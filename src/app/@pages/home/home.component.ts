import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CartService } from 'src/app/@services/cart/cart.service';
import { Item, ItemService } from 'src/app/@services/item/item.service';
import { UserService } from 'src/app/@services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  items: Item[] = [];
  busqueda = new FormControl("");
  allItems: Item[] = [];

  constructor(
    private userService: UserService,
    private router: Router,
    private itemService: ItemService,
    public cartService: CartService,
  ) { }

  ngOnInit() {
    this.itemService.getItems().then(items => {
      this.items = items
      this.allItems = items
      console.log(this.items)
    });
    this.busqueda.valueChanges.subscribe(texto => {
      console.log(texto)
      if (texto) {
        this.items = this.allItems.filter(item => item.titulo.toLowerCase().includes(texto.toLowerCase()))
      } else {
        this.items = this.allItems
      }

    })
  }

  logout() {
    this.userService.clearActiveUser();
    this.router.navigate(['/login'])
  }

  async poblar() {
    await this.itemService.poblarProductos()
    console.log('terminó el poblado')
  }
}
