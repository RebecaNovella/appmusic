import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first, firstValueFrom } from 'rxjs';
import { UserService } from 'src/app/@services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm = new FormGroup({
    'mail': new FormControl("", { nonNullable: true, validators: [Validators.required, Validators.email] }),
    'password': new FormControl("", { nonNullable: true, validators: Validators.required}),
  })

  constructor(
    private userService: UserService,
    private router: Router,
  ) {

  }

  async login() {
    const data = this.loginForm.getRawValue()
    const mail = data.mail;
    const password = data.password
    const valid = await firstValueFrom(this.userService.validateCredentials(mail, password))
    if (valid) {
      this.router.navigate(['/'])
    } else {
      //to do Mostrar un mensaje de credenciales invalidas
      console.log('fallo login')
    }

  }
}
