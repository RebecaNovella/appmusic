import { Component, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MetodoPagoService, Tarjeta } from 'src/app/@services/metodo-pago/metodo-pago.service';
import { UserService } from 'src/app/@services/user/user.service';

@Component({
  selector: 'app-metodo-pago',
  templateUrl: './metodo-pago.component.html',
  styleUrls: ['./metodo-pago.component.css']
})
export class MetodoPagoComponent {
  tarjetas: Tarjeta[] = []
  form = new FormGroup({
    nombre: new FormControl("", { nonNullable: true, validators: Validators.required }),
    numero: new FormControl("", { nonNullable: true, validators: Validators.required }),
    fechaExpiracion: new FormControl("", { nonNullable: true, validators: Validators.required }),
    cvv: new FormControl("", { nonNullable: true, validators: Validators.required }),
  })
  constructor(
    private metodoPago: MetodoPagoService,
    private modalService: NgbModal,
    private userService: UserService,
  ) {

  }

  ngOnInit() {
    this.loadTarjetas()
  }

  open(content: TemplateRef<any>) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {
        const tarjeta = this.form.getRawValue()
        this.metodoPago.addTarjeta(tarjeta, this.userService.getActiveUser()!.id!).subscribe(() => {
          this.loadTarjetas()
          this.form.reset()
        })

      },
      (reason) => {

      },
    );
  }

  loadTarjetas() {
    this.metodoPago.getTarjetas(this.userService.getActiveUser()!.id!).subscribe(tarjetas => this.tarjetas = tarjetas)
  }
}
