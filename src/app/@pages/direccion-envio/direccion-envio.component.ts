import { Component, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Direccion, DireccionEnvioService } from 'src/app/@services/direccion-envio/direccion-envio.service';

@Component({
  selector: 'app-direccion-envio',
  templateUrl: './direccion-envio.component.html',
  styleUrls: ['./direccion-envio.component.css']
})
export class DireccionEnvioComponent {
  envios: Direccion[] = []
  form = new FormGroup({
    nombre: new FormControl("", { nonNullable: true, validators: Validators.required }),
    pais: new FormControl("", { nonNullable: true, validators: Validators.required }),
    cp: new FormControl("", { nonNullable: true, validators: Validators.required }),
    ciudad: new FormControl("", { nonNullable: true, validators: Validators.required }),
    direccion: new FormControl("", { nonNullable: true, validators: Validators.required }),
  })
  constructor(
    private envioService: DireccionEnvioService,
    private modalService: NgbModal,
  ) {

  }

  ngOnInit() {
    this.loadDirecciones()
  }

  open(content: TemplateRef<any>) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				const envio = this.form.getRawValue()
        this.envioService.addDirEnvio(envio).subscribe(() => {
          this.loadDirecciones()
          this.form.reset()
        })
        
			},
			(reason) => {
				
			},
		);
	}

  loadDirecciones() {
    this.envioService.getEnvios().subscribe(envios => this.envios = envios)
  }
}
