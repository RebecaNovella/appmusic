import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from 'src/app/@services/cart/cart.service';
import { Item, ItemService } from 'src/app/@services/item/item.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.css']
})
export class AlbumDetailsComponent {
  nombreAlbum: string = "No se definió";
  album!: Item;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private itemService: ItemService,
    private cartService: CartService
  ) { }

  async ngOnInit() {
    this.nombreAlbum = this.route.snapshot.paramMap.get('album') || '';
    const albumData = await this.itemService.getItemByTitle(this.nombreAlbum);
    if (!albumData) {
      this.router.navigate(['']);
      return;
    }
    this.album = albumData;
  }

  addToCart(album: Item) {
    this.cartService.addToCart(album)
  }
}
