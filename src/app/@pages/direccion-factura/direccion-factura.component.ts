import { Component, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Direccion, DireccionEnvioService } from 'src/app/@services/direccion-envio/direccion-envio.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-direccion-factura',
  templateUrl: './direccion-factura.component.html',
  styleUrls: ['./direccion-factura.component.css']
})
export class DireccionFacturaComponent {
  facturas: Direccion[] = []
  form = new FormGroup({
    nombre: new FormControl("", { nonNullable: true, validators: Validators.required }),
    pais: new FormControl("", { nonNullable: true, validators: Validators.required }),
    cp: new FormControl("", { nonNullable: true, validators: Validators.required }),
    ciudad: new FormControl("", { nonNullable: true, validators: Validators.required }),
    direccion: new FormControl("", { nonNullable: true, validators: Validators.required }),
  })
  constructor(
    private facturaService: DireccionEnvioService,
    private modalService: NgbModal,
  ) {

  }

  ngOnInit() {
    this.loadDirecciones()
  }

  open(content: TemplateRef<any>) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				const factura = this.form.getRawValue()
        this.facturaService.addDirFactura(factura).subscribe(() => {
          this.loadDirecciones()
          this.form.reset()
        })
			},
			(reason) => {
				
			},
		);
	}

  loadDirecciones() {
    this.facturaService.getFacturas().subscribe(facturas => this.facturas = facturas)
  }
}
