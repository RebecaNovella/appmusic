import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DireccionFacturaComponent } from './direccion-factura.component';

describe('DireccionFacturaComponent', () => {
  let component: DireccionFacturaComponent;
  let fixture: ComponentFixture<DireccionFacturaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DireccionFacturaComponent]
    });
    fixture = TestBed.createComponent(DireccionFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
