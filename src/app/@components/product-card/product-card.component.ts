import { Component, Input } from '@angular/core';
import { CartService } from 'src/app/@services/cart/cart.service';
import { Item } from 'src/app/@services/item/item.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {
  @Input()
  album!: Item;

  constructor(
    private cartService: CartService,
  ) { }

  addToCart(item: Item) {
    this.cartService.addToCart(item)
  }
}
