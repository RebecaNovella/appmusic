import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';

export interface User {
  name: string;
  phone: string,
  mail: string,
  password: string,
  rol?: string,
  turno?: string,
  id?: number
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  activeUser?: User;
  url = 'http://localhost:5000'
  url_usuario = this.url + '/usuarios'
  url_login = this.url + '/login'

  constructor(
    private http: HttpClient
  ) { }

  addUser(user: User) {
    const body = {
      "nombre": user.name,
      "telefono": user.phone,
      "correo": user.mail,
      "clave": user.password,
      "rol": "CLIENTE",
      "turno": null
    }
    return this.http.post<any>(this.url_usuario, body)
  }

  validateCredentials(mail: string, password: string) {
    const body = {
      "correo": mail,
      "clave": password
    }
    return this.http.post<{ valid: boolean, usuario: any }>(this.url_login, body).pipe(
      tap(response => {
        if (response.valid) {
          const user = {
            name: response.usuario.nombre,
            phone: response.usuario.telefono,
            mail: response.usuario.correo,
            password: response.usuario.clave,
            rol: response.usuario.rol,
            turno: response.usuario.turno,
            id: response.usuario.id
          }
          this.setActiveUser(user)
        }
      }),
      map(response => response.valid)
    )
  }

  setActiveUser(user: User) {
    localStorage.setItem('activeUser', JSON.stringify(user))
  }

  clearActiveUser() {
    localStorage.removeItem('activeUser')
  }

  getActiveUser(): User | undefined {
    const data = localStorage.getItem('activeUser');
    if (data) {
      return JSON.parse(data)
    }
    return undefined
  }
}
