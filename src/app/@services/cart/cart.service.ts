import { Injectable } from '@angular/core';
import { Item } from '../item/item.service';




@Injectable({
  providedIn: 'root'
})
export class CartService {
  constructor() { }

  addToCart(item: Item) {
    const items = this.getLocalItems()
    items.push(item)
    this.saveLocalItems(items)
  }

  deleteFromCart(item: Item) {
    const items = this.getLocalItems()
    const indice = items.findIndex(a => item.titulo == a.titulo)
    items.splice(indice, 1)
    this.saveLocalItems(items)
  }

  clearCart() {
    const items: Item[] = []
    this.saveLocalItems(items)
  }

  getItems() {
    return this.getLocalItems()
  }

  getLocalItems(): Item[] {
    const data = localStorage.getItem('items');
    if (data) {
      return JSON.parse(data)
    }
    return []
  }

  saveLocalItems(items: Item[]) {
    localStorage.setItem('items', JSON.stringify(items))
  }
}
