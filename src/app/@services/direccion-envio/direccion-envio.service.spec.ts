import { TestBed } from '@angular/core/testing';

import { DireccionEnvioService } from './direccion-envio.service';

describe('DireccionEnvioService', () => {
  let service: DireccionEnvioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DireccionEnvioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
