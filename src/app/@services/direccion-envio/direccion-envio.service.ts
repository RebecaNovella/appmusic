import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { UserService } from '../user/user.service';

export interface Direccion {
  nombre: string,
  pais: string,
  cp: string,
  ciudad: string,
  direccion: string, 
  tipo?: string
}
@Injectable({
  providedIn: 'root'
})
export class DireccionEnvioService {
  url = 'http://localhost:5000/direcciones'

  constructor(
    private http: HttpClient,
    private userService: UserService,
  ) { }

  addDirEnvio(envio: Direccion) {
   return  this.saveDireccion(envio, 'envio')
  }

  getEnvios() {
    return this.getDireccion('envio')
  }

  addDirFactura(factura: Direccion) {
    return this.saveDireccion(factura, 'factura')
  }

  getFacturas() {
    return this.getDireccion('factura')
  }

  getDireccion(tipo: 'factura' | 'envio'): Observable<Direccion[]> {
    return this.http.get<Direccion[]>(this.url + '/' + this.getUserId()).pipe(
      map(direcciones => direcciones.filter(direccion => direccion.tipo == tipo))
    )
  }

  saveDireccion(direccion: Direccion, tipo: 'factura' | 'envio') {
    direccion.tipo = tipo
    const body = {
      ...direccion,
      usuario: this.getUserId()
    }
    return this.http.post(this.url, body)
  }

  getUserId() {
    return this.userService.getActiveUser()!.id!
  }
}
