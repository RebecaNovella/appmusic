import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, concatMap, first, firstValueFrom, from, map, of, tap } from 'rxjs';

export interface Review {
  "autor": string,
  "valoracion": number,
  "opinion": string,
}

export interface Item {
  "url_portada": string,
  "titulo": string,
  "artista": string,
  "precio": number,
  "valoracion": number
  "categoria": string,
  "reviews": Review[],
  "id"?: number,
}

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  productos: Item[] = [];
  url = 'http://localhost:5000/productos'

  constructor(
    private http: HttpClient,
  ) { }

  async getItems(): Promise<Item[]> {
    const items = await firstValueFrom(this.http.get<any>(this.url))
    for (let item of items) {
      const reviews = await firstValueFrom(this.getReviews(item.id))
      item.reviews = reviews
    }
    return items
  }

  getReviews(id_producto: number) {
    return this.http.get<Review[]>('http://localhost:5000/reviews/' + id_producto)
  }

  getLocalItems(): Observable<Item[]> {
    if (this.productos.length > 0) {
      return of(this.productos);
    }
    return this.http.get<Item[]>("./assets/ListaProductos.json")
      .pipe(
        tap(data => this.productos = data),
        map(data => data)
      );
  }

  async getItemByTitle(title: string) {
    const items = await this.getItems();
    return items.find(item => item.titulo == title);
  }

  async poblarProductos() {
    const items = await firstValueFrom(this.getLocalItems())
    console.log(items)
    for (let item of items) {
      //await firstValueFrom(this.saveItem(item))
      const products = await this.getItems()
      console.log(products)
      const product = products.find(p => p.titulo == item.titulo)!
      console.log(product)
      for (let review of item.reviews) {
        this.saveReview(review, product.id!).subscribe()
      }
    }
  }

  saveItem(item: Item) {
    return this.http.post(this.url, item)
  }

  saveReview(review: Review, product_id: number) {
    const body = {
      ...review,
      producto: product_id
    }
    return this.http.post('http://localhost:5000/reviews', body)
  }
}
