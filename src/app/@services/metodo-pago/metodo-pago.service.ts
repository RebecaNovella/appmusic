import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

export interface Tarjeta {
  nombre: string,
  numero: string,
  fechaExpiracion: string,
  cvv: string
}
@Injectable({
  providedIn: 'root'
})
export class MetodoPagoService {
  url = 'http://localhost:5000/pagos'

  constructor(
    private http: HttpClient
  ) { }

  addTarjeta(tarjeta: Tarjeta, id_usuario: number) {
    const body = {
      "usuario": id_usuario,
      "nombre": tarjeta.nombre,
      "num_tarjeta": tarjeta.numero,
      "fecha_exp": tarjeta.fechaExpiracion,
      "cvv": tarjeta.cvv
    }
    return this.http.post(this.url, body)
  }


  getTarjetas(id_usuario: number) {
    return this.http.get<any>(this.url + '/' + id_usuario).pipe(
      map(response => response.map((tarjeta: any) => ({
        nombre: tarjeta.nombre,
        numero: tarjeta.num_tarjeta,
        fechaExpiracion: tarjeta.fecha_exp,
        cvv: tarjeta.cvv
      })))
    )
  }
}
