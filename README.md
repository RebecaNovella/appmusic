# MusicApp

## Requisitos
- Opcional: Un ambiente de node y npm. Se puede usar nvm
- node versión 18 o superior
- npm versión 9.5 o superior
- angular versión 16 o superior

1. Para instalar angular se puede con el comando `npm install -g @angular/cli`
2. Para instalar las dependencias del proyecto `npm install`

## Ejecutar aplicación

Ejecutar `ng serve` para el servidor de desarrollo. Desde el navegador visitar `http://localhost:4200/login`, para visitar la app.

